var centre_image_pos_gauche = 0;
var centre_image_pos_gauche_init = 0;
var centre_image_pos_haut = 0;
var centre_image_pos_haut_init = 0;
var t_centre_actif;
var centre_image_x_init = 0;
var centre_image_x_max;
var centre_image_y_init = 0;
var centre_image_y_max;

$(document).on("click", ".lity_bloquer", function(){ return false;})

$(document).on("touchstart MSPointerDown pointerdown mousedown", "img.croix_centre_image", function(e){
	var event = e.originalEvent;
	event.preventDefault();
	event.stopPropagation();

	var t = $(this);

	// Casse-pied: bloquer le comportement de la lightbox Lity
	t.parents("a").addClass("lity_bloquer").removeClass("lity-enabled").removeClass("cboxElement");
	t_centre_actif = t;

	centre_image_pos_gauche_init = event.pageX || event.targetTouches[0].pageX;
	centre_image_pos_haut_init = event.pageY || event.targetTouches[0].pageY;

	centre_image_pos_gauche = centre_image_pos_gauche_init;
	centre_image_pos_haut = centre_image_pos_haut_init;

	centre_image_x_init = parseInt(t.css("left"));
	centre_image_x_max = t.parent().width() ;
	centre_image_y_init = parseInt(t.css("top"));
	centre_image_y_max = t.parent().height();


}).on("touchmove MSPointerMove pointermove mousemove", function(e){
	if (t_centre_actif) {
		var event = e.originalEvent;


		centre_image_pos_gauche = event.pageX || event.targetTouches[0].pageX;
		centre_image_pos_haut = event.pageY || event.targetTouches[0].pageY;

		var decal = parseInt(centre_image_pos_gauche) - parseInt(centre_image_pos_gauche_init);
		var decalY = parseInt(centre_image_pos_haut) - parseInt(centre_image_pos_haut_init);

		var x = centre_image_x_init+decal;
		x = Math.max(0, x);
		x = Math.min(x, centre_image_x_max);
		var y = centre_image_y_init+decalY;
		y = Math.max(0, y);
		y = Math.min(y, centre_image_y_max);
		t_centre_actif.css("left", x+"px").css("top", y+"px");
		event.preventDefault();

	}
}).on("touchend MSPointerUp pointerup mouseup", function(e){
	if (t_centre_actif) {
		var event = e.originalEvent;
		e.preventDefault();
		e.stopPropagation();

		var el = t_centre_actif.parent("a");
		var lien = el.attr("href");
		var url = lien.replace(/^\.\.\//, '');
		url = url.split('?').shift();


		var x = parseInt(t_centre_actif.css("left")) / centre_image_x_max;
		var y = parseInt(t_centre_actif.css("top")) / centre_image_y_max;

		x = Math.max(0, x);
		x = Math.min(1, x);
		y = Math.max(0, y);
		y = Math.min(1, y);

		$.ajax("index.php?action=centre_image_forcer&x="+x+"&y="+y+"&url="+url);

		t_centre_actif = false;
		setTimeout(function(){
			el.removeClass("lity_bloquer").addClass("lity-enabled").addClass("cboxElement");
		}, 100);



	}
})

function centre_image_croix(el, x, y) {
	if (el.find("img.croix_centre_image").length == 0) {
		el.css("display", "inline-block").css("position", "relative")
			.find("img").addClass("img_source").css("outline", "1px solid green").css("display", "block")
			.parent()
			// Dimensions : forcer la taille quels que soient les styles appliqué au parent
			.prepend("<img src='"+croix+"' class='croix_centre_image' width='24' height='24' style='width:24px; height:24px'>");
	}

	el.find("img.croix_centre_image").css("left", x+"px").css("top", y+"px");
}

function centre_image_calculer_croix(el) {
	var x = el.data('x');
	var y = el.data('y');
	x = x * el.find("img:not(.croix_centre_image)").width();
	y = y * el.find("img:not(.croix_centre_image)").height();
	// console.log(url + " / " + x + " / " + y);
	centre_image_croix(el, x, y);
}

jQuery.fn.centre_images_find_images_link = function() {
	var imagetypes = ['image/gif', 'image/png', 'image/jpeg', 'image/webp', 'image/avif'];
	var extensions = ['gif', 'png', 'jpg', 'webp', 'avif'];
	// les liens vers des images : ils ont type="image/xxx" (comme pour la mediabox)
	// (on regarde aussi les liens dont le href contient IMG/ pour ne rien rater, mais c'est un fallback)
	var $images = $(this).find('a[type^="image/"],a[href*="IMG/"]').not('[data-centre-image="off"]');
	$images = $images.filter(function(index, el) {
		var img = el.querySelector('img');
		if (!img) {
			return false;
		}
		if (img.width < 100 && img.height < 100) {
			return false;
		}
		var type = el.getAttribute('type');
		if (type && imagetypes.indexOf(type)) {
			return true;
		}
		var href= el.getAttribute('href');
		for (var i in extensions) {
			if (href.match(new RegExp('\\.'+extensions[i]+'$|\\.'+extensions[i]+'\\?'))) {
				return true;
			}
		}
		return false;
	});
	return $images;
}

jQuery.fn.centre_images = function() {
	var images = $(this).centre_images_find_images_link();
	images.not('.has-centre_image').each(function () {
		var $me = $(this);
		// recuperer l'URL sans les ../
		var lien = $me.attr("href");
		var url = lien.replace(/^\.\.\//, '');
		url = url.split('?').shift();


		if ($me.parents(".spip_documents").length == 0) {
			$me.attr("data-root-url", url);
		}
		$me.addClass('has-centre_image');

		$.getJSON("../index.php?page=centre_image_json&url=" + url,
			function (data) {
				$me.data('x', data.x);
				$me.data('y', data.y);
				centre_image_calculer_croix($me);
			}
		);
	});
}

jQuery.fn.centre_images_rafraichir = function() {
	var images = $(this).centre_images_find_images_link();
	images.each(function () {
		centre_image_calculer_croix($(this));
	});
}

function centre_image_afficher() {
	$('.portfolios, .formulaire_editer_logo .spip_logo, #documents_joints, .documents-album, .editer_apercu, .galerie .media-image,.galerie.media-image, [data-centre-image="on"]')
		.not('[data-centre-image="off"]').centre_images();
	$(".portfolios .liste_items.documents").on("affichage.documents.change", function(){
		$(this).centre_images_rafraichir();
	});
}


(function($){
	$(document).ready(centre_image_afficher);
	onAjaxLoad(centre_image_afficher);
})(jQuery);
