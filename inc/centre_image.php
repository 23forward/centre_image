<?php
/**
 * Fonctions utiles au plugin Centre image
 *
 * @plugin     Centre image
 * @copyright  2015
 * @author     ARNO*
 * @licence    GNU/GPL
 * @package    SPIP\Centre_image\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Calcule le chemin correct théorique du fichier
 *
 * - extrait l'URL d'une éventuel attribut 'src' d'une balise
 * - passe en url relative si c'était en absolu
 * - enlève un timestamp ou un token éventuel (accès restreint)
 *
 * @param string $fichier
 * return string
 */
function centre_image_preparer_fichier($fichier) {
	// nettoyer le fichier (qui peut être dans un <img>)
	if (strpos($fichier, "src=") !== false) {
		if (!function_exists('extraire_attribut')) {
			include_spip('inc/filtres');
		}
		$fichier = extraire_attribut($fichier, "src");
	}

	// Enlever timestamp ou token
	$fichier = explode('?', $fichier, 2);
	$fichier = array_shift($fichier);

	// si URL absolue de l'image, on passe en relatif
	if (tester_url_absolue($fichier)) {
		$url_site = url_de_base();
		if (strpos($fichier, $url_site) === 0) {
			$fichier = substr($fichier, strlen($url_site));
		}
	}

	return $fichier;
}

function centre_image_fichier_force($fichier) {
	$file_to_hash = $fichier;
	if (_DIR_RACINE and strpos($file_to_hash, _DIR_RACINE) === 0) {
		$file_to_hash = substr($file_to_hash, strlen(_DIR_RACINE));
	}
	$md5 = md5($file_to_hash);
	$dir_centre_force = sous_repertoire(_DIR_IMG, "cache-centre-image");
	$fichier_force_json = $dir_centre_force . $md5 . ".json";

	return $fichier_force_json;
}

function centre_image_fichier_cache($fichier, $famille_cache = '') {
	$file_to_hash = $fichier;
	if (_DIR_RACINE and strpos($file_to_hash, _DIR_RACINE) === 0) {
		$file_to_hash = substr($file_to_hash, strlen(_DIR_RACINE));
	}
	$md5 = md5($file_to_hash);
	$l1 = substr($md5, 0, 1);
	$l2 = substr($md5, 1, 1);

	$dir_cache = sous_repertoire(_DIR_VAR, "cache-centre-image" . ($famille_cache ? "-$famille_cache" : ''));
	$dir_cache = sous_repertoire($dir_cache, $l1);
	$dir_cache = sous_repertoire($dir_cache, $l2);
	$fichier_cache_json = $dir_cache . $md5 . ".json";

	return $fichier_cache_json;
}

function centre_image_lire_cache($fichier, $famille_cache = '') {
	$fichier_old_logo = false;
	if (strpos($fichier, 'logo/') !== false
	  and preg_match(",logo/(art|rub|aut|mot)(on|off),", $fichier)) {
		$fichier_old_logo = str_replace("logo/", "", $fichier);
	}

	// éviter plusieurs accès successifs
	$mtime_source = filemtime($fichier);


	$fichier_force_json = centre_image_fichier_force($fichier);
	if (file_exists($fichier_force_json)
	  and filemtime($fichier_force_json) >= $mtime_source
	  and $res = file_get_contents($fichier_force_json)) {
		$res = json_decode($res, true);
		return $res;
	}

	// est-ce qu'il y a un centre forcé sur le logo anciennement à la racine de _DIR_IMG ?
	if ($fichier_old_logo) {
		$fichier_old_force_json = centre_image_fichier_force($fichier_old_logo);
		if (file_exists($fichier_old_force_json)
		  and filemtime($fichier_old_force_json) >= $mtime_source
		  and $res = file_get_contents($fichier_old_force_json)) {
			@rename($fichier_old_force_json, $fichier_force_json);
			$res = json_decode($res, true);
			// renommer le vieux fichier au passage
			return $res;
		}
	}


	// est-ce qu'on a un PI forcé transmis depuis une image source ?
	$fichier_cache_json = centre_image_fichier_cache($fichier, 'force');
	if (file_exists($fichier_cache_json)
		and filemtime($fichier_cache_json) >= $mtime_source
		and $res = file_get_contents($fichier_cache_json)) {
		$res = json_decode($res, true);
		return $res;
	}

	$fichier_cache_json = centre_image_fichier_cache($fichier, $famille_cache);
	if (file_exists($fichier_cache_json)
		and filemtime($fichier_cache_json) >= $mtime_source
		and $res = file_get_contents($fichier_cache_json)) {
		$res = json_decode($res, true);
		return $res;
	}

	// on a pas trouvé de cache
	return false;
}

function centre_image_ecrire_cache($fichier, $res, $famille_cache = '') {
	$fichier_cache_json = centre_image_fichier_cache($fichier, $famille_cache);
	file_put_contents($fichier_cache_json, json_encode($res));
	return $fichier_cache_json;
}

function centre_image_ecrire_force($fichier, $res) {
	$fichier_force_json = centre_image_fichier_force($fichier);
	file_put_contents($fichier_force_json, json_encode($res));
	return $fichier_force_json;
}
